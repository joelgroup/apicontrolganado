<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Metodos de Usuario
Route::post('/auth/login',  'App\Http\Controllers\UsuarioController@login');

Route::post('/user/create',  'App\Http\Controllers\UsuarioController@create');

Route::post('/user/update',  'App\Http\Controllers\UsuarioController@update');

Route::post('/user/delete',  'App\Http\Controllers\UsuarioController@delete');

Route::post('/user/userAuth',  'App\Http\Controllers\UsuarioController@userAuth');

Route::get('/user/getUser/{id}',  'App\Http\Controllers\UsuarioController@getUser');






//Fin Metodos Usuario

//Metodos de Rol
Route::get('/rol/index',  'App\Http\Controllers\RolController@index');

//Fin Metodos de rol

//Metodos Vaca
Route::get('/vacas/index',  'App\Http\Controllers\VacaController@index');

Route::post('/vacas/create',  'App\Http\Controllers\VacaController@create');

Route::post('/vacas/update',  'App\Http\Controllers\VacaController@update');

Route::post('/vacas/delete',  'App\Http\Controllers\VacaController@delete');

Route::post('/vacas/inseminacion',  'App\Http\Controllers\VacaController@inseminacion');

Route::post('/vacas/inseminacionSuccess',  'App\Http\Controllers\VacaController@inseminacionSuccess');

Route::post('/vacas/inseminaciones',  'App\Http\Controllers\VacaController@inseminaciones');

Route::post('/vacas/vacasXleche',  'App\Http\Controllers\VacaController@vacasXleche');

Route::post('/vacas/guardarLeche',  'App\Http\Controllers\VacaController@guardarLeche');

Route::get('/vacas/reporteAnual',  'App\Http\Controllers\VacaController@reporteAnual');

Route::post('/vacas/reporteMensual',  'App\Http\Controllers\VacaController@reporteMensual');

Route::post('/vacas/reporteMensualIndividual',  'App\Http\Controllers\VacaController@reporteMensualIndividual');

Route::post('/tareas/index',  'App\Http\Controllers\TareasController@index');

Route::post('/tareas/cambiarEstatus',  'App\Http\Controllers\TareasController@cambiarEstatus');

Route::post('/tareas/indexSupervisor',  'App\Http\Controllers\TareasController@indexSupervisor');

Route::get('/tareas/indexObreros',  'App\Http\Controllers\UsuarioController@indexObreros');

Route::post('/tareas/create',  'App\Http\Controllers\TareasController@create');

Route::get('/vacas/obtenerVaca/{id}',  'App\Http\Controllers\VacaController@obtenerVaca');

Route::get('/vacas/razas',  'App\Http\Controllers\VacaController@razas');

Route::post('/vacas/liberar',  'App\Http\Controllers\VacaController@liberar');

Route::post('/vacas/medicamento/create',  'App\Http\Controllers\MedicamentoController@create');


Route::post('/vacas/medicamento/medicamentos',  'App\Http\Controllers\MedicamentoController@medicamentos');





























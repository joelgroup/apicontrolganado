<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class lecheXdias extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'lechexdias';
    public $fillable = [
        'idvaca',
        'cantidadLts',
        'fecha'
    ];



}

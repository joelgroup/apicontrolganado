<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'usuario';
    public $fillable = [
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'codigoEmpleado',
        'password',
        'idRol'
    ];

    protected $appends = ['rolName'];


    public function getRolNameAttribute() 
    { 
        return $this->rol->descripcion;
    }

    public function rol(){
        return $this->BelongsTo('App\Models\Rol','idRol','id') -> withDefault();
        
    } 






}

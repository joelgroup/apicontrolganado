<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tareas extends Model
{
    use HasFactory;



    use HasFactory;
    protected $table = 'tareasasignada';
    public $fillable = [
        'id'	,
        'idUsuario'	,
        'idSupervisor'	,
        'estado'	,
        'descripcion',
    ];

    protected $appends = ['estatus'];


    public function usuario(){
        return $this->BelongsTo('App\Models\Usuario','idUsuario','id') -> withDefault();
        
    }


    public function supervisor(){
        return $this->BelongsTo('App\Models\Usuario','idSupervisor','id') -> withDefault();
        
    }

    public function getEstatusAttribute() 
    {
        if($this->estado == 1){
            return 'Asignado';
        }else if($this->estado == 2){
            return 'En proceso';
        }else if ($this -> estado == 3){
            return 'Finalizado';
        }
        
    }


    
    
    

}

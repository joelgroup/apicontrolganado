<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inseminacion extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'inseminaciones';
    public $fillable = [
        'idVaca',
        'fecha',
        'resultado',
        'liberada'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vaca extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'vacas';

    public $fillable=[
        'id',
        'codigoVaca',
        'pesoKgs',
        'idRaza',
        'fechaNac',
        'viva',
        'fechaMuerte'
    ];
    
    protected $appends = ['razaName','ultimaInseminacion','prenada'];


    public function getRazaNameAttribute() 
    { 
        return $this->raza->descripcion;
    }

    public function raza(){
        return $this->BelongsTo('App\Models\Raza','idRaza','id') -> withDefault();
        
    } 
    
    public function  getUltimaInseminacionAttribute(){
        $fecha = 'App\Models\Inseminacion'::select('fecha') ->where('idVaca',$this->id)->where('liberada', null) ->where('resultado' , null)->orderBy('fecha','desc')->first();
        
        if($fecha)
            $fecha = $fecha-> fecha ;
        else
            $fecha = '';
        return  $fecha; 
    } 
    
    public function  getPrenadaAttribute(){
        $resultado = 'App\Models\Inseminacion'::select('resultado') ->where('idVaca',$this->id)->where('liberada', null)->orderBy('fecha','desc')->first();
        
        if($resultado)
            $resultado = $resultado-> resultado ;
        else
            $resultado = 0;
        return  $resultado; 
    } 





}

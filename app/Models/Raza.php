<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Raza extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'razas';
    public $fillable = [
        'descripcion'
    ];
    
    
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Vaca;
use App\Models\Inseminacion;
use App\Models\lecheXdias;
use App\Models\Raza;


use Carbon\Carbon;



use DB;




class VacaController extends Controller
{
    //

    public function index(){

        return Vaca::where('viva',1)->get();
    }

    public function liberar(Request $request){

        $inseminacion = Inseminacion::where('idVaca',$request->id)->where('resultado', 1)->orderBy('fecha','desc')->first();




        $inseminacion=Inseminacion::findOrFail($inseminacion->id);

        $inseminacion->update([
            'liberada' => 1
        ]);


        $vaca = Vaca::where('id', $request->id)->first();

        return $vaca;
    }



    public function create(Request $request){
        


        //return ['resultado' => true , 'msg' => $request->fechaNac];
        
        $data =Validator::make($request->all(),[
            'codigoVaca' => 'required|max:8|min:8|unique:vacas',
            'pesoKgs' => 'required|numeric',
            'fechaNac' => 'required|date',
            'idRaza' => 'required|numeric'
        ]);
        
        if($data -> fails()){
             return  ['resultado'=>false , 'msg' => $data ->errors()];
        }

        try {
            Vaca::create($request->all());
            return ['resultado' => true , 'msg' => 'La vaca quedo registrado con éxito'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al registrar una vaca, comunicate con el administrador'];
            
        }

    }

    

    public function update(Request $request){

        
        $data =Validator::make($request->all(),[
            'pesoKgs' => 'required',
            'fechaNac' => 'required',
            'idRaza' => 'required'
        ]);
        
        if($data -> fails()){
             return  ['resultado'=>false ,  $data ->errors()];
        }

        try {
            $vaca = Vaca::findOrFail($request->id);

            $vaca -> update(
                $request->all()
            );

            return ['resultado' => true , 'msg' => 'La vaca quedo editada con éxito'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al registrar una vaca, comunicate con el administrador'];
            
        }

    }

    
    public function delete(Request $request){

        $data =Validator::make($request->all(),[
            'viva' => 'required',
            'id' => 'required'
           
        ]);
        
        if($data -> fails()){
             return  ['resultado'=>false ,  $data ->errors()];
        }

        try {
            $vaca = Vaca::findOrFail($request->id);

            $vaca -> update([
                'viva' => $request -> viva,
                'fechaMuerte' =>  Carbon::now()
                ]

            );

            return ['resultado' => true , 'msg' => 'La vaca quedo eliminada'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al borrar una vaca, comunicate con el administrador'];
            
        }

    }

    public function lechexdia(Request $request){

        
        $data =Validator::make($request->all(),[
            'idvaca' => 'required',
            'cantidadLts' => 'required',
            'fecha' => 'required'
           
        ]);
        
        if($data -> fails()){
             return  ['resultado'=>false ,  $data ->errors()];
        }

        try {
            $vaca = Vaca::findOrFail($request->id);

            $vaca -> delete();

            return ['resultado' => true , 'msg' => 'La vaca quedo eliminada'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al borrar una vaca, comunicate con el administrador'];
            
        }

    }
    
    public function inseminacion(Request $request){

        
        $data =Validator::make($request->all(),[
            'idVaca' => 'required',
            'fecha' => 'required',
           
        ]);
        
        if($data -> fails()){
             return  ['resultado'=>false ,  $data ->errors()];
        }

        try {
            Inseminacion::create($request->all());
            

            return ['resultado' => true , 'msg' => 'La vaca quedo Inseminada'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al borrar una vaca, comunicate con el administrador'];
            
        }

    }

    
    public function inseminacionSuccess(Request $request){

        
        $data =Validator::make($request->all(),[
            'idVaca' => 'required',
           
        ]);
        
        $inseminacion = Inseminacion::where('idVaca' , $request->idVaca)->orderBy('fecha', 'desc')->first();
        
        if($data -> fails()){
             return  ['resultado'=>false ,  $data ->errors()];
        }

        try {
            $inseminacion = Inseminacion::findOrFail($inseminacion->id);
            $inseminacion->update(['resultado' => 1]);
             

            return ['resultado' => true , 'msg' => 'La vaca quedo embarazada'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al borrar una vaca, comunicate con el administrador'];
            
        }

    }

    public function inseminaciones(Request $request){

        
        $data =Validator::make($request->all(),[
            'idVaca' => 'required',
           
        ]);
        
        $inseminacion = Inseminacion::where('idVaca' , $request->idVaca)->orderBy('fecha', 'desc')->get();
        
        if($data -> fails()){
             return  ['resultado'=>false ,  $data ->errors()];
        }

        try {
            return ['resultado' => true , 'inseminaciones' =>  $inseminacion ];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al borrar una vaca, comunicate con el administrador'];
            
        }

    }

    public function razas(){
        return Raza::get();
    }

    public function vacasXleche( Request $request){

        $sql = " select razas.descripcion as razaName,vacas.id as idVaca,vacas.codigoVaca,vacas.idRaza,vacas.pesoKgs,lechexdias.cantidadLts,lechexdias.id as idleche,lechexdias.fecha 
        from vacas 
        left join lechexdias on vacas.id=lechexdias.idVaca and lechexdias.fecha ='$request->fecha' 
        inner join razas on vacas.idRaza = razas.id
        where vacas.viva = 1     ";

        $lechexDias = DB::select($sql );
        
        return [ 'result' => $lechexDias];
    }

    public function guardarLeche(Request $request){
        $vacas = $request -> vacas;

        foreach($vacas as $vaca){


            $lechexdias = lecheXdias::where("fecha",$request-> fecha) -> where('idVaca' , $vaca['idVaca']) -> first();

            if($lechexdias){
                $leche = lecheXdias::findOrFail($vaca['idleche']);
                $leche -> update([
                    'cantidadLts'=> $vaca['cantidadLts']
                ]);
            }else{

                lecheXdias::create(
                    [
                        'idvaca' => $vaca['idVaca'],
                        'cantidadLts' => $vaca ['cantidadLts'],
                        'fecha' => $request->fecha
                    ]
                    );
            
            }



        }

        return [ 'resultado' => true , 'msg' => 'Se guardo correctamente'];

    }

    public function reporteAnual(){

        $lecheXAno = DB:: select("
        SELECT 
        SUM(IF(YEAR(fecha)=2022,cantidadLts,0)) As 'L_2022', 
        SUM(IF(YEAR(fecha)=2021,cantidadLts,0)) As 'L_2021', 
        SUM(IF(YEAR(fecha)=2020,cantidadLts,0)) As 'L_2020', SUM(IF(YEAR(fecha)=2019,cantidadLts,0)) As 'L_2019', 
        SUM(IF(YEAR(fecha)=2018,cantidadLts,0)) As 'L_2018', SUM(IF(YEAR(fecha)=2017,cantidadLts,0)) As 'L_2017' 
        FROM lechexdias where YEAR(fecha) between 2017 and 2022
        ");


        return ['reporteAnual' => $lecheXAno];




    }



    public function reporteMensual(Request $request){

        $lecheMens = DB:: select(
            "
            SELECT 
                SUM(IF(MONTH(fecha)=01,cantidadLts,0)) As 'enero', 
                SUM(IF(MONTH(fecha)=02,cantidadLts,0)) As 'febrero', 
                SUM(IF(MONTH(fecha)=03,cantidadLts,0)) As 'marzo', 
                SUM(IF(MONTH(fecha)=04,cantidadLts,0)) As 'abril', 
                SUM(IF(MONTH(fecha)=05,cantidadLts,0)) As 'mayo', 
                SUM(IF(MONTH(fecha)=06,cantidadLts,0)) As 'junio', 
                SUM(IF(MONTH(fecha)=07,cantidadLts,0)) As 'julio', 
                SUM(IF(MONTH(fecha)=08,cantidadLts,0)) As 'agosto', 
                SUM(IF(MONTH(fecha)=09,cantidadLts,0)) As 'septiembre', 
                SUM(IF(MONTH(fecha)=10,cantidadLts,0)) As 'octubre', 
                SUM(IF(MONTH(fecha)=11,cantidadLts,0)) As 'noviembre', 
                SUM(IF(MONTH(fecha)=12,cantidadLts,0)) As 'diciembre'
                FROM lechexdias where YEAR(fecha) = $request->fecha
            "
        );


        return ['lecheMens' => $lecheMens];

    }


    public function reporteMensualIndividual(Request $request){

        $lecheMens = DB:: select(
            "
            SELECT 
                SUM(IF(MONTH(fecha)=01,cantidadLts,0)) As 'enero', 
                SUM(IF(MONTH(fecha)=02,cantidadLts,0)) As 'febrero', 
                SUM(IF(MONTH(fecha)=03,cantidadLts,0)) As 'marzo', 
                SUM(IF(MONTH(fecha)=04,cantidadLts,0)) As 'abril', 
                SUM(IF(MONTH(fecha)=05,cantidadLts,0)) As 'mayo', 
                SUM(IF(MONTH(fecha)=06,cantidadLts,0)) As 'junio', 
                SUM(IF(MONTH(fecha)=07,cantidadLts,0)) As 'julio', 
                SUM(IF(MONTH(fecha)=08,cantidadLts,0)) As 'agosto', 
                SUM(IF(MONTH(fecha)=09,cantidadLts,0)) As 'septiembre', 
                SUM(IF(MONTH(fecha)=10,cantidadLts,0)) As 'octubre', 
                SUM(IF(MONTH(fecha)=11,cantidadLts,0)) As 'noviembre', 
                SUM(IF(MONTH(fecha)=12,cantidadLts,0)) As 'diciembre'
                FROM lechexdias inner join vacas on vacas.id = lechexdias.idVaca
                where YEAR(fecha) = $request->fecha and vacas.codigoVaca= '$request->codigoVaca'            "
        );


        return ['lecheMens' => $lecheMens];

    }

    public function obtenerVaca($id){

        $vaca = Vaca::where('id', $id)-> first();


        return $vaca;


    }

    
}



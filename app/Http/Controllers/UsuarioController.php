<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Validator;


class UsuarioController extends Controller
{
    //

    public function login(Request $request){
        // Numero de empleado -> numempleado
        // Contraseña  -> password
        $usuario = Usuario::where('codigoEmpleado',$request->numempleado)->where('password',$request->password) -> where('activo',1)->first();

        return $usuario;

    }

    public function getUser( $id){
        $usuario=Usuario::where('id' , $id)-> first();
        

        return $usuario;
    }



    public function indexObreros(Request $request){
        // Numero de empleado -> numempleado
        // Contraseña  -> password
        $usuarios = Usuario::where('idRol',2)-> where('activo', 1)->get();

        return $usuarios;

    }

    public function userAuth(Request $request){
        // Numero de empleado -> numempleado
        // Contraseña  -> password
        $usuario = Usuario::where('codigoEmpleado',$request->numempleado)->first();

        return $usuario;

    }

  

    public function create(Request $request){
       

        $data =Validator::make($request->all(),[
            'nombre' => 'required',
            'apellido_paterno'        => 'required',
            'apellido_materno' => 'required',
            'codigoEmpleado'     => 'required|max:8|min:8|unique:usuario',
            'password' => 'required|min:8',
            'idRol'   => 'required',
        ]);
        
        if($data -> fails()){
             return  ['resultado'=>false ,  'msg' => $data ->errors()];
        }


        try {
            Usuario::create($request->all());
            return ['resultado' => true , 'msg' => 'El usuario quedo registrado con éxito'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al registrar un usuario, comunicate con el administrador'];
            
        }


    }

    public function update(Request $request){
 
        
        $data =Validator::make($request->all(),[
            'nombre' => 'required',
            'apellido_paterno'        => 'required',
            'apellido_materno' => 'required',
            'password' => 'required|min:8|max:20',
            'idRol'   => 'required'
        ]);
        
        if($data -> fails()){
            return  ['resultado'=>false ,  'msg' => $data ->errors()];
       }

        try {
            $usuario = Usuario::findOrFail($request->id);

            $usuario -> update(
                $request->all()
            );

            return ['resultado' => true , 'msg' => 'El usuario quedo editado con éxito'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al registrar un usuario, comunicate con el administrador'];
            
        }


    }


    public function delete(Request $request){
 
        $data =Validator::make($request->all(),[
            'id' => 'required'
        ]);
        

        try {
            $usuario = Usuario::findOrFail($request->id);

            $usuario -> update([
                'activo' => 2
            ]);

            return ['resultado' => true , 'msg' => 'El usuario quedo eliminado'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al registrar un usuario, comunicate con el administrador'];
            
        }

        
        


    }


}

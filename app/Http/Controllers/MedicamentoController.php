<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Medicamento;

class MedicamentoController extends Controller
{
    //

    public function create ( Request $request){
        $data =Validator::make($request->all(),[
            'fecha' => 'required',
            'nombreMedicamento'=> 'required',
            'idVaca' => 'required'
          
        ]);
        
        if($data -> fails()){
             return  ['resultado'=>false ,  'msg' => $data ->errors()];
        }


        try {
            Medicamento::create($request->all());
            return ['resultado' => true , 'msg' => 'El Medicamento quedo registrado con éxito'];

        }catch (Exception  $e){

            return ['resultado' => false , 'msg' => 'Ocurrio un error al registrar un Medicamento, comunicate con el administrador'];
            
        }
    }

    public function medicamentos( Request $request){
        $medicamentos = Medicamento:: where('idVaca' , $request->idVaca)->get();

        return $medicamentos;
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tareas;


class TareasController extends Controller
{
    //


    public function index(Request $request){

        
        $tareas = Tareas::where('idUsuario', $request->idusuario)->with('usuario', 'supervisor')->get();


        return $tareas;


    }

    public function indexSupervisor(Request $request){

        
        $tareas = Tareas::where('idSupervisor', $request->idusuario)->with('usuario', 'supervisor')->get();


        return $tareas;


    }

    public function cambiarEstatus(Request $request){

        
        


        $tarea = Tareas::findOrFail($request->id);


        $tarea->update([
            'estado' => $request -> estado
        ]);




        return ['resultado' => true , 'msg'=> 'Se realizo el cambio'];


    }

    
    public function create(Request $request){

        
        $tarea = Tareas::create([
            'idUsuario' => $request -> idObrero	,
            'idSupervisor'	=> $request -> idSupervisor,
            'estado' => 1	,
            'descripcion' => $request -> descripcion,
        ]);

        $tarea2 = Tareas::where('id', $tarea -> id) ->with('usuario', 'supervisor')-> first();


        return $tarea2;


    }





}

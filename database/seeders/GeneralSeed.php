<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Usuario;
use App\Models\Rol;


class GeneralSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Usuario::create([
            'nombre' => 'Joe' ,
            'apellido_paterno'=> 'Doe',
            'apellido_materno' => 'Smith',
            'codigoEmpleado' => '90188458',
            'password' => '123',
            'idRol' => '1'
        ]);

        Rol::create([
            'descripcion' => 'Administrador'
        ]);

        
        Rol::create([
            'descripcion' => 'Obrero'
        ]);
    }
}
